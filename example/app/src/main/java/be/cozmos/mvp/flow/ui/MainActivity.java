package be.cozmos.mvp.flow.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import be.cozmos.mvp.R;
import be.cozmos.mvp.network.MainProxyImpl;
import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity implements MainPresenter.View {

    private static MainPresenter mainPresenter;

    @BindView(R.id.editText) EditText editText;
    @BindView(R.id.button) Button validationButton;
    @BindView(R.id.countries) TextView countriesTextView;
    @BindView(R.id.progressbar) ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (mainPresenter == null) {
            mainPresenter = new MainPresenterImpl(new MainProxyImpl());
        }

        mainPresenter.attach(this);
        mainPresenter.loadCountries();

        validationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.validate(editText.getText().toString());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detach();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayCountries(final String countryCodes) {
        countriesTextView.setText(countryCodes);
        countriesTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showValidationError(final String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

}