package be.cozmos.mvp.network;

import java.util.List;

import be.cozmos.mvp.model.Country;

public interface MainProxy {

    void fetch(Callback callback);

    interface Callback {
        void success(List<Country> countries);
    }

}
