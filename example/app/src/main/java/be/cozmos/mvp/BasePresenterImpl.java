package be.cozmos.mvp;

public class BasePresenterImpl<T> implements BasePresenter<T> {

    protected T view;

    @Override
    public void attach(T view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public boolean hasView() {
        return view != null;
    }

}