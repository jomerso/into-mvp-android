package be.cozmos.mvp.model;

public final class Country {

    private String name;
    private String shortCode;

    public Country(
            final String name,
            final String shortCode) {

        this.name = name;
        this.shortCode = shortCode;
    }

    @Override
    public String toString() {
        return name + " (" + shortCode + ")";
    }

}