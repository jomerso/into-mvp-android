package be.cozmos.mvp;

public interface BasePresenter<T> {

    void attach(T view);

    void detach();

    boolean hasView();

}