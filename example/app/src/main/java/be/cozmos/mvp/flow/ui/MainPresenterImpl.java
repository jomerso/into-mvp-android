package be.cozmos.mvp.flow.ui;

import android.text.TextUtils;

import java.util.List;

import be.cozmos.mvp.BasePresenterImpl;
import be.cozmos.mvp.model.Country;
import be.cozmos.mvp.network.MainProxy;

public final class MainPresenterImpl extends BasePresenterImpl<MainPresenter.View>
        implements MainPresenter {

    private MainProxy mainProxy;

    public MainPresenterImpl(final MainProxy mainProxy) {
        this.mainProxy = mainProxy;
    }

    @Override
    public void validate(final String input) {
        if (hasView()
                && "".equals(input)) {

            view.showValidationError("Invalid input!");
        }
    }

    @Override
    public void loadCountries() {
        if (hasView()) {
            view.showLoading();
        }

        mainProxy.fetch(new MainProxy.Callback() {
            @Override
            public void success(final List<Country> countries) {
                if (hasView()) {
                    view.displayCountries(TextUtils.join(", ", countries));
                    view.dismissLoading();
                }
            }
        });
    }

}