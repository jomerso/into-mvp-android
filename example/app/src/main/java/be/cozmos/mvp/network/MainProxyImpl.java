package be.cozmos.mvp.network;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import be.cozmos.mvp.model.Country;

public final class MainProxyImpl implements MainProxy {

    @Override
    public void fetch(final Callback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final List<Country> countries = new ArrayList<>();
                countries.add(new Country("Belgium", "BE"));
                countries.add(new Country("France", "FR"));

                callback.success(countries);
            }
        }, 5000);
    }


}

