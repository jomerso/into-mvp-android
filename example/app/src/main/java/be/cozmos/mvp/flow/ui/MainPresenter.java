package be.cozmos.mvp.flow.ui;

import be.cozmos.mvp.BasePresenter;

public interface MainPresenter extends BasePresenter<MainPresenter.View> {

    void loadCountries();

    void validate(String input);

    interface View {

        void showLoading();

        void dismissLoading();

        void displayCountries(String countryCodes);

        void showValidationError(String errorMessage);

    }

}