package be.cozmos.mvp.flow.ui;

import android.os.Handler;
import android.text.TextUtils;

import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import be.cozmos.mvp.model.Country;
import be.cozmos.mvp.network.MainProxy;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MainPresenterImpl.class, Handler.class, Runnable.class, TextUtils.class})
public class MainPresenterTest {

    private MainPresenter mainPresenter;
    private MainProxy mainProxy;
    private MainPresenter.View view;

    @Before
    public void setUp() {
        mainProxy = PowerMock.createMock(MainProxy.class);
        view = PowerMock.createMock(MainPresenter.View.class);
        mainPresenter = new MainPresenterImpl(mainProxy);
    }

    @After
    public void cleanup() {
        PowerMock.verifyAll();
        PowerMock.resetAll();
    }

    @Test
    public void isValidTextInput_emptyString() {
        view.showValidationError(EasyMock.anyString());

        PowerMock.replayAll();

        mainPresenter.attach(view);
        mainPresenter.validate("");
    }

    @Test
    public void isValidTextInput_veryLongString() {
        PowerMock.replayAll();

        mainPresenter.attach(view);
        mainPresenter.validate("ThisIsAVeryLongString");
    }

    @Test
    public void loadCountries_success() throws Exception {
        final List<Country> mockedCountries = new ArrayList<>();

        view.showLoading();
        view.displayCountries("CountryValue");
        view.dismissLoading();

        mainProxy.fetch(EasyMock.<MainProxy.Callback>anyObject());
        EasyMock.expectLastCall().andAnswer(new IAnswer<Boolean>() {
                    @Override
                    public Boolean answer() throws Throwable {
                        final Object[] arguments = EasyMock.getCurrentArguments();
                        ((MainProxy.Callback) arguments[0]).success(mockedCountries);
                        return null;
                    }
                });

        PowerMock.mockStatic(TextUtils.class);
        EasyMock.expect(TextUtils.join(", ", mockedCountries)).andReturn("CountryValue");

        PowerMock.replayAll();

        mainPresenter.attach(view);
        mainPresenter.loadCountries();
    }

}